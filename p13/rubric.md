# Project 13 (P13) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Outputs not visible/did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. (-3)
- Import statements are not mentioned in the required cell at the top of the notebook. (-3)

### Question specific guidelines:

- `bar_plot` (2)
	- data is not plotted correctly (-1)
	- legend is not deleted or axes are not properly labeled (-1)

- `scatter_plot` (2)
	- data is not plotted correctly (-1)
	- legend is not deleted or axes are not properly labeled (-1)

- `horizontal_bar_plot` (1)
	- data is not plotted correctly (-1)

- `pie_plot` (1)
	- data is not plotted correctly (-1)

- `get_regression_coeff` (1)
	- function logic is incorrect (-1)

- `get_regression_line` (1)
	- function logic is incorrect (-1)

- `regression_line_plot` (2)
	- `get_regression_line` function is not used to answer (-1)
	- function does not create correct scatter plot or the correct line plot using `df["fit"]` (-1)

- `conn` (3)
	- data structure is defined more than once (-1)
	- did not close the connection to `conn` at the end (-2)

- q1 (3)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-1)

- q2 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q3 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q4 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q5 (6)
	- incorrect logic is used to answer (-2)
	- `bar_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q6 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `bar_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q7 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q8 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `scatter_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q9 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `scatter_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q10 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `scatter_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q11 (3)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-1)

- q12 (3)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-1)

- q13 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q14 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q15 (4)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)

- q16 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `horizontal_bar_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q17 (6)
	- did not use SQL to answer (-2)
	- incorrect logic is used to answer (-2)
	- `regression_line_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q18 (4)
	- incorrect logic is used to answer (-2)
	- `get_regression_line` function is not used to answer (-1)
	- `regression_line_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)

- q19 (2)
	- incorrect logic is used to answer (-1)
	- `get_regression_coeff` function is not used to answer (-1)

- q20 (4)
	- `pie_plot` function is not used to plot (-1)
	- plot is not properly labeled (-1)


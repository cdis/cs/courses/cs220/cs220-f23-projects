# Project 6 (P6) grading rubric

## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Used concepts/modules such as dictionaries, or pandas not covered in class yet - built-in functions that you have been introduced to can be used. (-3)
- Import statements are not all placed at the top of the notebook. (-1)
- Hardcoded answers. (all points allotted for that question)

### Question specific guidelines:

- `cell` (5)
	- variables `csv_data`, `csv_header`, and `csv_rows` are not defined as expected (-1)
	- function does not return `None` when data is missing (-1)
	- function does not typecast to `int` or `float` when expected (-1)
	- function logic is incorrect (-1)
	- `process_csv` function is not used to read `power_generators.csv` (-1)

- q1 (4)
	- not all rows are looped through (-1)
	- incorrect logic is used (-1)
	- `cell` function is not used to read data (-1)

- q2 (3)
	- not all rows are looped through (-1)
	- incorrect logic is used (-1)
	- `cell` function is not used to read data (-1)

- q3 (3)
	- not all rows are looped through (-1)
	- incorrect logic is used (-1)
	- `cell` function is not used to read data (-1)

- `find_entities_with_phrase` (4)
	- not all rows are looped through (-1)
	- case insensitive matches are not considered (-1)
	- duplicates are not removed (-1)
	- `cell` function is not used to read data (-1)

- q4 (2)
  - `find_entities_with_phrase` function is not used to answer (-2)

- q5 (4)
	- incorrect logic is used to answer (-2)
	- `cell` function is not used to read data (-1)

- q6 (4)
	- incorrect logic is used to answer (-2)
	- `cell` function is not used to read data (-1)

- q7 (5)
	- not all rows are looped through (-1)
	- the output is not sorted correctly (-1)
	- incorrect logic is used (-1)
	- `cell` function is not used to read data (-1)

- q8 (5)
	- not all rows are looped through (-1)
	- the output is not sorted correctly (-1)
	- incorrect comparison operators are used (-1)
	- incorrect logic is used (-1)
	- `cell` function is not used to read data (-1)

- `num_generators_by` (3)
	- not all rows are looped through (-1)
	- function logic is incorrect (-1)
	- `cell` function is not used to read data (-1)

- q9 (2)
	- `num_generators_by` function is not used to answer (-2)

- q10 (3)
	- incorrect logic is used to answer (-1)
	- `num_generators_by` function is not used to answer (-1)
	- `find_entities_with_phrase` function is not used to answer (-1)

- q11 (5)
  - all entities are looped through instead of just the unique entities (-2)
	- incorrect logic is used to answer (-1)
	- `num_generators_by` function is not used to answer (-1)

- `find_indices_within` (4)
	- not all rows are looped through (-1)
	- incorrect comparison operators are used (-1)
	- function logic is incorrect (-1)
	- `cell` function is not used to read data (-1)

- q12 (3)
	- length of the list is found by looping through it (-1)
	- `find_indices_within` function is not used to answer (-2)

- q13 (3)
	- duplicates are not removed from the list (-1)
	- incorrect logic is used to answer (-1)
	- `find_indices_within` function is not used to answer (-1)

- q14 (5)
	- incorrect logic is used to answer (-2)
	- `find_indices_within` function is not used to answer (-1)
	- `cell` function is not used to read data (-1)

- `median` (3)
	- function modifies the original input list (-1)
	- function logic is incorrect when inputs are of odd length (-1)
	- function logic is incorrect when inputs are of even length (-1)

- q15 (4)
	- incorrect logic is used to answer (-1)
	- `find_indices_within` function is not used to answer (-1)
	- `median` function is not used to answer (-1)
	- `cell` function is not used to read data (-1)

- `total_summer_capacity_of` (4)
	- not all rows are looped through (-1)
	- missing data is not ignored (-1)
	- function logic is incorrect (-1)
	- `cell` function is not used to read data (-1)

- q16 (2)
	- `total_summer_capacity_of` function is not used to answer (-2)

- q17 (5)
  - all power plants are looped through instead of just the unique power plants (-1)
	- incorrect logic is used to answer (-1)
	- `total_summer_capacity_of` function is not used to answer (-1)
	- `median` function is not used to answer (-1)

- `avg_winter_capacity_of` (4)
	- not all rows are looped through (-1)
	- missing data is not ignored (-1)
	- function logic is incorrect (-1)
	- `cell` function is not used to read data (-1)

- q18 (2)
  - `avg_winter_capacity_of` function is not used to answer (-2)

- q19 (5)
	- all technologies are looped through instead of just the unique technologies (-1)
  - list of unique technologies is recomputed (-1)
	- incorrect logic is used to answer (-1)
	- `avg_winter_capacity_of` function is not used to answer (-1)

- q20 (4)
  - all technologies are looped through instead of just the unique technologies (-1)
	- incorrect logic is used to answer (-1)
	- `avg_winter_capacity_of` function is not used to answer (-1)

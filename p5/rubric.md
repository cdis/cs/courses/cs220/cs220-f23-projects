# Project 5 (P5) grading rubric

## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Functions are defined more than once. (-3)
- Import statements are not all placed at the top of the notebook. (-1)
- Used concepts or modules not covered in class yet. (-5)
- Hardcoded answers. (all points allotted for that question)

### Question specific guidelines:

- q1 (2)
	- required function is not used (-1)

- q2 (3)
	- required function is not used (-1)

- q3 (4)
	- index of the last hurricane is hardcoded (-3)
	- required function is not used (-1)

- q4 (4)
  - incorrect logic is used to answer (-2)
  - number of hurricanes in the dataset is hardcoded (-1)

- q5 (4)
- incorrect logic is used to answer (-2)
- number of hurricanes in the dataset is hardcoded (-1)

- `format_damage` (4)
  - function output is incorrect when the damage has suffix `K` (-1)
	- function output is incorrect when the damage has suffix `M` (-1)
	- function output is incorrect when the damage has suffix `B` (-1)
	- function output is incorrect when the damage has no suffix (-1)

- q6 (4)
	- did not exit loop and instead iterated further after finding the hurricane (-2)
	- number of hurricanes in the dataset is hardcoded (-1)
	- `format_damage` function is not used to convert the damages into an integer (-1)

- q7 (4)
	- incorrect logic is used to answer (-2)
	- number of hurricanes in the dataset is hardcoded (-1)
	- `format_damage` function is not used to convert the damages into an integer (-1)

- q8 (4)
	- incorrect logic is used to answer (-2)
	- number of hurricanes in the dataset is hardcoded (-1)
	- `format_damage` function is not used to convert the damages into an integer (-1)

- q9 (4)
	- incorrect logic is used to answer (-2)
	- tie breaking is not implemented correctly (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- q10 (4)
	- incorrect logic is used to answer (-2)
	- tie breaking is not implemented correctly (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- `get_year` (2)
	- function logic is incorrect (-2)

- `get_month` (2)
	- function logic is incorrect (-2)

- `get_day` (2)
	- function logic is incorrect (-2)

- q11 (5)
  - variable to store the index or name of the earliest hurricane is not initialized as `None` (-1)
	- `get_year` function is not used to determine the year of formation (-1)
	- used indices of the hurricanes to determine the earliest hurricane (-1)
	- hurricanes with damages <= 1B are not ignored (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- q12 (5)
	- variable to store the index or name of the most recent hurricane is not initialized as `None` (-1)
	- `get_year` function is not used to determine the year of formation (-1)
	- used indices of the hurricanes to determine the most recent hurricane (-1)
	- hurricanes with damages <= 100B are not ignored (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- `deadliest_in_range` (4)
  - variable to store the index of the deadliest hurricane is not initialized as `None` (-1)
	- function does not consider all hurricanes active between `year1` and `year2` (-1)
	- number of hurricanes in the dataset is hardcoded (-1)
	- function logic is incorrect (-1)

- q13 (4)
  - functions `deadliest_in_range` and `format_damage` are not used to answer (-2)
	- incorrect logic is used to answer (-2)

- q14 (4)
	- function `deadliest_in_range` is not used to answer (-2)
	- incorrect logic is used to answer (-2)

- q15 (4)  
	- functions `get_year` and `get_month` are not used to answer (-1)
	- incorrect logic is used to answer (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- `get_year_total` (4)
	- function logic is incorrect (-2)
	- function `get_year` is not used to answer (-1)
	- number of hurricanes in the dataset is hardcoded (-1)

- q16 (4)
	- function `get_year_total` is not used to answer (-3)

- q17 (5)
  - function `get_year_total` is not used to answer (-2)
	- did not loop through the years in the last decade and hardcoded all ten years (-2)
	- incorrect logic is used to answer (-1)

- q18 (5)
  - `year_with_most_hurricanes` is not initialized as some year in the twentieth century, or as `None` (-2)
	- function `get_year_total` is not used to determine the year with the most hurricanes (-1)
	- tie breaking is not implemented correctly (-1)
	- incorrect logic is used to answer (-1)

- q19 (4)
  - hurricanes that formed at the end of one year and dissipated at the end of the next are not considered (-1)
	- incorrect logic is used to answer (-1)
	- function `get_month` is not used to answer (-1)

- q20 (5)
  - years with no deadliest hurricane are not ignored (-1)
	- all hurricanes formed between 2001 and 2023 are not considered (-1)
	- incorrect logic is used to answer (-1)
	- functions `deadliest_in_range` and `format_damage` are not used to answer (-1)

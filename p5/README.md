# Project 5 (P5): Investigating Hurricane Data


## Corrections:

* **(10/5/2023 - 12:00 PM):** The file `hurricanes.csv` has been replaced with a typo fixed. This typo caused the public test for Question 19 to fail for some students. Please redownload the `hurricanes.csv` file if you faced those issues. You **do not** have to redownload the `p5.ipynb` notebook.

## Clarifications:

* **(10/5/2023 - 12:00 PM):** In `deadliest_in_range`, if **all** of the hurricanes within the given year range caused `0` deaths, you are expected to return the index of the first hurricane in the dataset within that year range.

* **(10/5/2023 - 12:00 PM):** In Question 15, the **average** refers to the **yearly average**. As in, the numerator is supposed to be the number of hurricanes formed in October between the given years, and the denominator is supposed to be the number of years between the given years.

* **(10/5/2023 - 12:00 PM):** In Question 20, even though there are **no** hurricanes in the dataset from the year `2023`, to answer the question as it is asked, you are **still** expected to **loop** through that year, and to **skip** it after checking that it has no deadliest hurricane.

**Find any issues?** Report to us:

- Takis Chytas <chytas@wisc.edu>
- Samuel Guo <sguo258@wisc.edu>

## Note on Academic Misconduct:
You are **allowed** to work with a partner on your projects. While it is not required that you work with a partner, it is **recommended** that you find a project partner as soon as possible as the projects will get progressively harder. Be careful **not** to work with more than one partner. If you worked with a partner on Lab-P5, you are **not** allowed to finish your project with a different partner. You may either continue to work with the same partner, or work on P5 alone. Now may be a good time to review our [course policies](https://cs220.cs.wisc.edu/f23/syllabus.html).

## Instructions:

This project will focus on **loops** and **strings**. To start, download `p5.ipynb`, `project.py`, `public_tests.py` and `hurricanes.csv`.

**Note:** Please go through [Lab-P5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p5) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p5.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

After you've downloaded the file to your `p5` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p5.ipynb`, `project.py`, `public_tests.py`, and `hurricanes.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p5/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P5 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P5 zip file.

   <img src="images/add_group_member.png" width="400">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p5.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). **Ignore** the `-/100.00` that is displayed to the right. You should be able to see both PASS / FAIL results for the 20 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">

- You can view your **final score** at the **end of the page**. If you pass all tests, then you will receive **full points** for the project. Otherwise, you can see your final score in the **summary** section of the test results (as in the image below):

   <img src="images/summary.png" width="400">

   If you want more details on why you lost points on a particular test, you can scroll up to find more details about the test.

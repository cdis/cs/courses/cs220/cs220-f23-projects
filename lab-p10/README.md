# Lab-P10: Files and Namedtuples

In this lab, you'll get practice with files and namedtuples, in preparation for P10.

-----------------------------
## Corrections/Clarifications


**Find any issues?** Please report to us:

- Ashwin Maran <amaran@wisc.edu>

------------------------------
## Learning Objectives

In this lab, you will practice...
* loading data in json files
* loading data in csv files
* using try/except to handle malformed data

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start
working on Lab-P10 with one person, then do the project with a different partner. Now may be a
good time to review [our course policies](https://cs220.cs.wisc.edu/f23/syllabus.html).

**Important:** P10 and P11 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partner up with someone for P10, you have to sustain that partnership until end of P11.
**You must acknowledge that you have read this to your lab TA**.

------------------------------

## Segment 1: Setup

Create a `lab-p10` directory and download the following files into the `lab-p10` directory.

* `small_data.zip`
* `lab-p10.ipynb`
* `public_tests.py`

After downloading data.zip, make sure to extract it (using [Mac directions](http://osxdaily.com/2017/11/05/how-open-zip-file-mac/) or [Windows directions](https://support.microsoft.com/en-us/help/4028088/windows-zip-and-unzip-files)). After extracting, you need to make sure that the project files are stored in the following structure:

```
+-- lab-p10.ipynb
+-- public_tests.py
+-- small_data
|   +-- .DS_Store
|   +-- .ipynb_checkpoints
|   +-- mapping_1.json
|   +-- mapping_2.json
|   +-- mapping_3.json
|   +-- planets_1.csv
|   +-- planets_2.csv
|   +-- planets_3.csv
|   +-- stars_1.csv
|   +-- stars_2.csv
|   +-- stars_3.csv
```

Make sure that the files inside `small_data.zip` are inside the `small_data` directory. You may delete `small_data.zip` after extracting these files from it.


## Segment 2:
For the remaining segments, detailed instructions are provided in `lab-p10.ipynb`. From the terminal, open a `jupyter notebook` session, open your `lab-p10.ipynb`, and follow the instructions in `lab-p10.ipynb`.

## Project 10

You can now get started with [p10](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p10). **You may copy/paste any code created here in project P10**. Have fun!

# Project 12 (P12) Grading Rubric


## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must **review** the rubric and make sure that you have followed the instructions provided in the project correctly.
- If you **fail** the **public tests** for a function or **hardcode** the answers to that question, you will automatically lose **all** points for that question.

## Rubric

### General guidelines:

- Outputs not visible/did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. (-3)
- Used concepts/modules such as `csv.DictReader`, and `numpy`, not covered in class yet. Note that built-in functions that you have been introduced to can be used. (-3)
- Large outputs are displayed in the notebook when they are unnecessary. (-3)
- Import statements are not mentioned in the required cell at the top of the notebook. (-3)

### Question specific guidelines:

- `download` (6)
	- downloading the file even if it already exists (-5)
	- function is defined more than once (-1)

- `rankings` (1)
	- data structure is defined incorrectly (-1)

- q1 (2)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q2 (3)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q3 (3)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q4 (3)
	- recomputed variable defined in Question 3 (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q5 (3)
	- recomputed variable defined in Question 3 (-1)
	- conditional statements or loops are used (-1)

- q6 (3)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q7 (4)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q8 (4)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q9 (4)
	- incorrect logic is used to answer (-1)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q10 (4)
	- incorrect logic is used to answer (-1)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q11 (5)
	- incorrect logic is used to answer (-2)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- `year_2021_ranking_df` (1)
	- data structure is defined incorrectly (-1)

- `year_2022_ranking_df` (1)
	- data structure is defined incorrectly (-1)

- `year_2023_ranking_df` (1)
	- data structure is defined incorrectly (-1)

- `institutions_df` (4)
	- data structure is defined incorrectly (-3)
	- data structure is defined more than once (-1)

- q12 (4)
	- `institutions_df` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q13 (5)
	- incorrect logic is used to answer (-1)
	- `institutions_df` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- q14 (4)
	- `institutions_df` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q15 (5)
	- incorrect logic is used to answer (-1)
	- `year_2021_ranking_df` data structure is not used to answer (-1)
	- conditional statements or loops are used (-1)

- q16 (5)
	- incorrect logic is used to answer (-1)
	- `year_2021_ranking_df` and `year_2023_ranking_df` data structures are not used to answer (-1)
	- conditional statements or loops are used (-1)

- q17 (5)
	- incorrect logic is used to answer (-1)
	- `rankings` data structure is not used to answer (-1)
	- conditional statements or loops are used (-2)

- q18 (5)
	- header is hardcoded (-1)
	- incorrect logic is used to answer (-2)

- `parse_html` (6)
	- function does not typecast based on columns (-1)
	- function does not assign `None` for missing data (-1)
	- function logic is incorrect (-3)
	- function is defined more than once (-1)

- q19 (4)
	- incorrect logic is used to answer (-1)
	- `parse_html` function is not used to answer (-2)

- q20 (5)
	- `parse_html` function is not used to answer (-3)


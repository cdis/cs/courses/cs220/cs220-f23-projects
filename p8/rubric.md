# Project 8 (P8) grading rubric

## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Outputs not visible/did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. (-3)
- Used concepts/modules such as `csv.DictReader` and `pandas` not covered in class yet. Note that built-in functions that you have been introduced to can be used. (-3)
- Large outputs such as `movies` are displayed in the notebook. (-3)
- Import statements are not mentioned in the required cell at the top of the notebook. (-1)

### Question specific guidelines:

- `get_mapping` (3)
	- function logic is incorrect (-2)
	- function is defined more than once (-1)

- q1 (2)
	- `get_mapping` function is not used to answer (-2)

- q2 (3)
	- `small_mapping` data structure is not used to answer (-2)
	- answer uses loops to iterate over dictionary keys (-1)

- q3 (3)
	- answer does not check only for keys that begin with *nm* (-1)
	- `small_mapping` data structure is not used to answer (-2)

- q4 (4)
	- answer does not exclude people whose first name or middle name is Murphy (-1)
	- answer does not exclude people whose last name contains Murphy as a substring (-1)
	- answer does not exclude movie titles (-1)
	- `small_mapping` data structure is not used to answer (-1)

- `get_raw_movies` (5)
	- function logic is incorrect (-3)
	- column indices are hardcoded instead of using the header to identify column names (-1)
	- function is defined more than once (-1)

- q5 (2)
	- `get_raw_movies` function is not used to answer (-2)

- q6 (4)
	- answer uses loops to find number of cast members (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- incorrect logic is used to answer (-1)
	- `raw_small_movies` data structure is not used to answer (-1)

- q7 (3)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `raw_small_movies` data structure is not used to answer (-1)

- `get_movies` (8)
	- function is called more than once with the same dataset (-4)
	- function logic is incorrect (-2)
	- `get_mapping` and `get_raw_movies` functions are not used (-1)
	- function loops through IDs in the `mapping_path` file (-1)

- q8 (2)
	- `get_movies` function is not used to answer (-2)

- `small_movies` (3)
	- data structure is defined incorrectly (-2)
	- data structure is redefined or modified after its initialization (-1)

- q9 (4)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `small_movies` data structure is not used to answer (-2)

- q10 (3)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `small_movies` data structure is not used to answer (-1)

- q11 (3)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `small_movies` data structure is not used to answer (-1)

- `movies` (5)
	- data structure is defined incorrectly (-3)
	- data structure is redefined or modified after its initialization  (-2)

- q12 (3)
	- `movies` data structure is not used to answer (-2)

- q13 (3)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `movies` data structure is not used to answer (-1)

- `find_specific_movies` (2)
	- function is redefined or defined more than once (-2)

- q14 (4)
	- `find_specific_movies` function is not used to answer (-4)

- q15 (4)
	- `find_specific_movies` function is not used to answer (-4)

- `bucketize_by_genre` (4)
	- function logic is incorrect (-2)
	- function is called more than once with the same input list (-2)

- `genre_dict` (5)
	- data structure is defined incorrectly (-3)
	- `bucketize_by_genre` function is not used to answer (-1)
	- data structure is redefined or modified after its initialization (-1)

- q16 (4)
	- incorrect logic is used to answer (-1)
	- answer uses loops to find number of unique movie genres (-1)
	- `genre_dict` data structure is not used to answer (-2)

- q17 (4)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `genre_dict` data structure is not used to answer (-1)

- q18 (4)
	- correct comparison operator is not used (-1)
	- incorrect logic is used to answer (-1)
	- answer uses loops to iterate over dictionary keys (-1)
	- `genre_dict` data structure is not used to answer (-1)

- q19 (4)
	- only movies that *Blake Lively* was `cast` in are considered (-1)
	- movies other than those involving *Blake Lively* are considered (-1)
	- incorrect logic is used to answer (-1)

- q20 (4)
	- incorrect logic is used to answer (-2)

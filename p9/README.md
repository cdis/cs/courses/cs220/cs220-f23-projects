# Project 9 (P9): Analyzing the Movies


## Corrections and clarifications:

* **(11/1/2023) - 7:00 PM**: Bug in `public_tests.py` that caused Q2 and Q9 to sometimes give the wrong output has been fixed. Please redownload `public_tests.py` if the earlier file caused any issues with those questions.

**Find any issues?** Report to us:

- Yuheng Wu <wu459@wisc.edu>
- Aboli Pai <apai7@wisc.edu>

## NOTE:

Please go through [`Lab-P9`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p9) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

## Instructions:

This project will focus on **bucketizing**, **plotting** and **sorting**. To start, download [`p9.ipynb`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p9/p9.ipynb), [`public_tests.py`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p9/public_tests.py), [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/raw/main/p9/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/raw/main/p9/mapping.csv).

If it takes too long to load the files `movies.csv` and `mapping.csv` on GitLab, you can directly download the file from these links: [`movies.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/raw/main/p9/movies.csv), and [`mapping.csv`](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/raw/main/p9/mapping.csv). You will need to **Right Click**, and click on the **Save as...** button to save the file through this method.

**Important:** Note that these files `movies.csv` and `mapping.csv` are the same files that you worked with in P8. So, you may just copy/paste the files from your P8 directory if you wish to.

You will work on `p9.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p9/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P9 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P9 zip file.

   <img src="images/add_group_member.png" width="400">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p9.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). **Ignore** the `-/50.00` that is displayed to the right. You should be able to see both PASS / FAIL results for the 10 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">

- You can view your **final score** at the **end of the page**. If you pass all tests, then you will receive **full points** for the project. Otherwise, you can see your final score in the **summary** section of the test results (as in the image below):

   <img src="images/summary.png" width="400">

   If you want more details on why you lost points on a particular test, you can scroll up to find more details about the test.

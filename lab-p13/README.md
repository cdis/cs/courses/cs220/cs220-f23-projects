# Lab-P13: SQL Databases and Plotting

In this lab, you'll learn to use SQL queries to extract data from a database. You will also write various plotting functions to visualize the extracted data.

-----------------------------
## Corrections/Clarifications


**Find any issues?** Please report to us:

- Ashwin Maran <amaran@wisc.edu>

## Learning Objectives:

## Learning Objectives

In this lab, you will practice how to:

* use SQL queries to extract data from a database,
* use different SQL keywords to organize data,
* write different plotting functions to visualize data and engage in data exploration.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start
working on Lab-P13 with one person, then do the project with a different partner. Now may be a
good time to review [our course policies](https://cs220.cs.wisc.edu/f23/syllabus.html).

**Important:** P12 and P13 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partnered up with someone for P12, you have to sustain that partnership until the end of P13.

------------------------------

## Segment 0: Setup

Unlike previous labs, you will **not** be working on an Otter notebook in this lab. Most importantly, this means that `public_tests.py` will **not** be provided to you. There will be `assert` statements in your `lab-p13.ipynb` notebook to guide you, but they will **not** be comprehensive. Instead, if you come across any syntactical or semantic errors, you will have to debug your code by yourself. Feel free to reach out to your TA or PM if you get stuck anywhere. and you will instead learn how to test your code by yourself.

You **will** however be provided with a `public_tests.py` for the project.

First, create a `lab-p13` directory and download the `lab-p13.ipynb` file into the directory.

## Segments 1-3: Web Requests, Caching, DataFrames and Scraping

For the remaining segments, detailed instructions are provided in `lab-p13.ipynb`. From the terminal, open a `jupyter notebook` session, open your `lab-p13.ipynb` and follow the instructions in `lab-p13.ipynb`.

## Project 13

You can now get started with [P13](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p13). **You may copy/paste any code created here in project P13**. Remember to only work on P13 with your partner from this point on. Have fun!
